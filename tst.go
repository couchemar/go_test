package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"github.com/kardianos/service"
	"log"
	"net/http"
)
import _ "github.com/denisenkom/go-mssqldb"

var logger service.Logger

type httpServer struct {
	db *sql.DB
}

type Object struct {
	Number  int    `json:"number"`
	Name    string `json:"name"`
	Address string `json:"address"`
}

type Objects struct {
	Objects []Object `json:"objects"`
}

func (server *httpServer) objects(respWriter http.ResponseWriter, req *http.Request) {

	if req.Method == "GET" {
		server.getObjects(respWriter, req)
	} else if req.Method == "PUT" {
		server.newObject(respWriter, req)
	} else {
		respWriter.WriteHeader(http.StatusNotImplemented)
		respWriter.Write([]byte(" Only GET and PUT"))
	}
}

func (server *httpServer) getObjects(respWriter http.ResponseWriter, r *http.Request) {

	rows, err := server.db.Query("select number, name, address from dbo.objects")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var objects []Object

	for rows.Next() {
		var number int
		var name, address string
		if err := rows.Scan(&number, &name, &address); err != nil {
			log.Fatal(err)
		}
		object := Object{number, name, address}
		objects = append(objects, object)
	}

	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	obj := Objects{objects}

	json.NewEncoder(respWriter).Encode(obj)
}

type newObjectT struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

func (server *httpServer) newObject(respWriter http.ResponseWriter, req *http.Request) {

	decoder := json.NewDecoder(req.Body)

	var obj newObjectT
	err := decoder.Decode(&obj)
	if err != nil {
		log.Fatal(err)
	}

	_, err = server.db.Exec("insert into dbo.objects(name, address) values(?1, ?2)", obj.Name, obj.Address)

	if err != nil {
		log.Fatal(err)
	}
}

type program struct{}

func (p *program) Start(s service.Service) error {
	go p.run()
	return nil
}
func (p *program) run() {
	db, err := sql.Open("mssql", "server=localhost;user id=user;password=123456;database=test")

	if err != nil {
		log.Fatal(err)
	}

	server := &httpServer{db}

	http.HandleFunc("/objects", server.objects)
	http.ListenAndServe(":8080", nil)
}
func (p *program) Stop(s service.Service) error {
	return nil
}

func main() {
	svcFlag := flag.String("service", "", "Control the system service.")
	flag.Parse()

	svcConfig := &service.Config{
		Name:        "GoService",
		DisplayName: "Go Service",
		Description: "Super duper service",
	}

	prg := &program{}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		log.Fatal(err)
	}
	logger, err = s.Logger(nil)
	if err != nil {
		log.Fatal(err)
	}
	if len(*svcFlag) != 0 {
		err := service.Control(s, *svcFlag)
		if err != nil {
			log.Printf("Valid actions: %q\n", service.ControlAction)
			log.Fatal(err)
		}
		return
	}
	err = s.Run()
	if err != nil {
		logger.Error(err)
	}
}
